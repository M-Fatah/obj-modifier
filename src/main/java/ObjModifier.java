import java.nio.FloatBuffer;

public class ObjModifier {
    public static void centerVertices(FloatBuffer buf) {
        // We often get 3D models which are not positioned at the origin of the coordinate system. Placing such models
        // on a 3D map is harder for users. One solution is to reposition such a model so that its center is at the
        // origin of the coordinate system. Your task is to implement the centering.
        //
        // FloatBuffer stores the vertices of a mesh in a continuous array of floats (see below)
        // [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]
        // This kind of layout is common in low-level 3D graphics APIs.
        // TODO: Implement your solution here.

        float minX = Float.MAX_VALUE, maxX = -Float.MAX_VALUE;
        float minY = Float.MAX_VALUE, maxY = -Float.MAX_VALUE;
        float minZ = Float.MAX_VALUE, maxZ = -Float.MAX_VALUE;

        float centerX = 0, centerY = 0, centerZ = 0;

        float[] verts = new float[buf.capacity()];

        int i = 0;
        while (buf.hasRemaining()) {
            // Get x coords.0
            float x = buf.get();
            verts[i++] = x;

            if(x > maxX)
                maxX = x;
            if(x < minX)
                minX = x;

            // Get y coords.
            float y = buf.get();
            verts[i++] = y;

            if(y > maxY)
                maxY = y;
            if(y < minY)
                minY = y;

            // Get z coords.
            float z = buf.get();
            verts[i++] = z;

            if(z > maxZ)
                maxZ = z;
            if(z < minZ)
                minZ = z;
        }

        // Calculate center.
        centerX = (minX + maxX) * 0.5f;
        centerY = (minY + maxY) * 0.5f;
        centerZ = (minZ + maxZ) * 0.5f;

        // Clear the FloatBuffer.
        buf.clear();

        // Re-center the object at the origin (0, 0, 0).
        for (int j = 0; j < verts.length; j += 3) {
            verts[j + 0] = verts[j + 0] - centerX;
            verts[j + 1] = verts[j + 1] - centerY;
            verts[j + 2] = verts[j + 2] - centerZ;

            //System.out.println(verts[j] + " " + verts[j + 1] + " " + verts[j + 2]);
        }

        // Fill the buffer again.
        buf.put(verts);
        // Rewind the buffer so the index starts at 0.
        buf.rewind();

        //System.out.println("Center: " + centerX + " " + centerY + " " + centerZ);
    }
}